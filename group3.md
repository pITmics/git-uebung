Exercise:

Explain the following commands with one sentence:

    $ git diff                        Shows the changes made
    $ git diff --staged                    Shows the difference between files in the staged area
    $ git diff test.txt                    Shows changes made in test.txt
    $ git diff --theirs                    Shows difference relative to the not HEAD branch
    $ git diff --ours                    Shows difference relative to the current branch
    $ git log                        Shows a history of the last commits
    $ git log --oneline                    The same as log, but in one line
    $ git log --oneline --all                The same as log --oneline, but for all branches
    $ git log --oneline --all --graph            Graphical representation of the commits in all branches in one line
    $ git log --oneline --all --graph --decorate    Prints additionally ref names of the commits
    $ git log --follow -p -- filename            Entire history of one file before and after renames
    $ git log -S'static void Main'            Searches for the string 'static void Main'
    $ git log --pretty=format:"%h - %an, %ar : %s"    Prints a formatted log: %h - abbreviated commit hash; %an - author name; %ar - realative author date; %s - subject

Example:

    $ git status         Shows the current state of the repository

Add you answers to the file readme.md!
